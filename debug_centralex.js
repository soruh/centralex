const net = require("net");

const package_names = {
  0x03: "End",
  0x04: "Reject",
  0x81: "RemConnect",
  0x82: "RemConfirm",
  0x83: "RemCall",
  0x84: "RemAck",
};

let socket = net.connect({ host: "localhost", port: 11818 });

let buffer = Buffer.alloc(0);

let ascii = false;

socket.on("data", (data) => {
  if (ascii) {
    process.stdout.write(data);
  } else {
    buffer = Buffer.concat([buffer, data]);

    while (buffer.length >= 2 && buffer.length >= 2 + buffer[1]) {
      [package, buffer] = [
        buffer.slice(0, 2 + buffer[1]),
        buffer.slice(2 + buffer[1]),
      ];

      if (package[0] == 4) {
        console.log(
          package_names[package[0]] + ': "' + package.slice(2).toString() + '"'
        );
        continue;
      }

      console.log(
        package_names[package[0]]
          ? require("util")
              .format(package.slice(2))
              .replace("Buffer >", "Buffer>")
              .replace("Buffer", package_names[package[0]])
          : package
      );
    }
  }
});

socket.on("close", () => {
  console.log("[close]");
  process.exit();
});

process.stdin.on("close", () => {
  socket.end();
});

process.stdin.on("data", (data) => {
  if (ascii) {
    socket.write(data);
  } else {
    data = data.toString();
    switch (data.slice(0, 3)) {
      case "end":
        sendEnd();
        break;
      case "rej":
        sendReject();
        break;
      case "con":
        sendConnect();
        break;
      case "ack":
        sendAcknowledge();
        break;
      case "typ":
        ascii = true;
        console.log("now typing");

        process.stdout.write(buffer);
        buffer = Buffer.alloc(0);
        break;
    }
  }
});

function sendEnd() {
  socket.write(Buffer.from([0x03, 0x00]));
}
function sendReject() {
  console.log("rejecting remote");
  socket.write(
    Buffer.from([
      0x04,
      0x0e,
      0x73,
      0x6f,
      0x6d,
      0x65,
      0x20,
      0x72,
      0x65,
      0x61,
      0x73,
      0x6f,
      0x6e,
      0x0d,
      0x0a,
      0x00,
    ])
  );
}
function sendConnect() {
  socket.write(Buffer.from([0x81, 0x06, 0x39, 0x30, 0x00, 0x00, 0xd2, 0x04]));
}
function sendAcknowledge() {
  socket.write(Buffer.from([0x84, 0x00]));
}
