use super::*;

pub struct CancelableTask<T> {
    pub handle: task::JoinHandle<T>,
    pub cancel: Sender<()>,
}
impl<T> CancelableTask<T> {
    pub fn new(task: Box<dyn FnOnce(Receiver<()>) -> task::JoinHandle<T>>) -> Self {
        let (cancel, receiver) = channel(1);
        let handle = task(receiver);
        Self { cancel, handle }
    }
    pub async fn cancel(&mut self) {
        trace!("canceling a CancelableTask");
        self.cancel.send(()).await
    }

    pub async fn terminate(mut self) -> T {
        trace!("terminating a CancelableTask");
        self.cancel().await;
        self.handle.await
    }
}
