use super::*;

pub struct Client {
    socket: TcpStream,
}
impl Client {
    #[throws(anyhow::Error)]
    pub async fn receive_package(&mut self) -> Package {
        let mut header = [0_u8; 2];

        let n = self.socket.peek(&mut header).await?;
        if n < 2 {
            if n == 0 {
                throw!(CentralexError::Disconnected);
            } else {
                throw!(io::Error::from(io::ErrorKind::UnexpectedEof));
            };
        }

        debug!("package header: {:?}", header);

        let [_package_type, package_length] = header;

        let mut buffer = vec![0_u8; package_length as usize + 2];

        self.socket.read_exact(&mut buffer).await?;

        debug!("package body: {:?}", &buffer);

        debug!("starting deserialize");

        let package = Package::deserialize_le(&mut std::io::Cursor::new(buffer))?;

        debug!("received package: {:?}", package);

        package
    }

    #[throws(io::Error)]
    pub async fn send_package(&mut self, package: impl Into<Package>) {
        let package = package.into();
        debug!("sending package: {:?}", package);

        let mut buffer: Vec<u8> = Vec::with_capacity(2);
        let mut cursor = std::io::Cursor::new(&mut buffer);

        package.serialize_le(&mut cursor)?;

        self.socket.write_all(&buffer).await?;
    }

    pub fn spawn(socket: TcpStream) -> ClientHandle {
        let (sender, client_receiver) = channel(16);
        let (client_sender, receiver) = channel(16);

        let handle = task::spawn(async move {
            let mut client = Client { socket };
            let sender = client_sender;
            let receiver = client_receiver;

            let mut timeout = Box::pin(task::sleep(HANDSHAKE_TIMEOUT).fuse());
            let mut heartbeat_timeout = Box::pin(task::sleep(HEARTBEAT_INTERVAL).fuse());

            loop {
                futures::select! {
                    package = receiver.recv().fuse() => {
                        if let Some(package) = package {
                            client.send_package(package).await?;
                        } else {
                            break;
                        }
                    },
                    package = client.receive_package().fuse() => {
                        timeout = Box::pin(task::sleep(HANDSHAKE_TIMEOUT).fuse());

                        let package = package?;
                        if let Package::Heartbeat (_) = package {
                            debug!("got heartbeat");
                        } else {
                            sender.send(package).await;
                        }
                    }
                    _ = timeout => throw!(CentralexError::Timeout),
                    _ = heartbeat_timeout => {
                        heartbeat_timeout = Box::pin(task::sleep(HEARTBEAT_INTERVAL).fuse());
                        client.send_package(Heartbeat {}).await?;
                        debug!("sent heartbeat");
                    },

                }
            }

            Ok(client.socket)
        });

        ClientHandle {
            number: None,
            sender: Some(sender),
            receiver,
            handle: Some(handle),
        }
    }
}

// TODO: propagate Error reason (is that possible/reasonable?)
pub struct ClientHandle {
    pub number: Option<ClientNumber>,
    sender: Option<Sender<Package>>,
    receiver: Receiver<Package>,
    handle: Option<JoinHandle<anyhow::Result<TcpStream>>>,
}

impl Drop for ClientHandle {
    fn drop(&mut self) {
        debug!("dropping a `ClientHandle`");
        if self.handle.is_some() {
            match task::block_on(self.terminate()) {
                Ok(_) => info!("shut down socket successfully"),
                Err(err) => error!("failed to shut down socket {:?}", err),
            }
        } else {
            debug!("client has already been shut down");
        }
    }
}

impl ClientHandle {
    pub async fn shutdown(&mut self) {
        debug!("telling client to finish");
        self.sender.take();
    }
    // TODO: impl Future
    #[throws(anyhow::Error)]
    pub async fn wait(&mut self) -> TcpStream {
        if let Some(handle) = self.handle.take() {
            debug!("waiting for client to finish");
            handle.await?
        } else {
            debug!("client was already finished");
            throw!(anyhow!("already finished"))
        }
    }
    #[throws(anyhow::Error)]
    pub async fn finish(&mut self) -> TcpStream {
        self.shutdown().await;
        let res = self.wait().await;
        debug!("client finished: {:?}", res.as_ref().map(|_| ()));
        res?
    }

    // TODO: remove if not needed
    // NOTE: `Drop`ing a Socket should surfice to close it,
    // but the I-Telex does weird stuff and might need extra delays or something
    #[throws(anyhow::Error)]
    pub async fn terminate(&mut self) {
        debug!("terminating client");
        let socket = self.finish().await?;

        debug!("shutting down client socket");
        socket.shutdown(std::net::Shutdown::Both)?;
    }
    #[throws(anyhow::Error)]
    pub async fn send_package(&self, package: impl Into<Package>) {
        let package = package.into();
        if let Some(ref sender) = self.sender {
            sender.send(package).await;
        } else {
            throw!(anyhow!("failed to send package"));
        }
    }
    pub async fn receive_package(&self) -> Option<Package> {
        self.receiver.recv().await
    }

    #[throws(anyhow::Error)]
    pub async fn reject(&mut self, message: impl Into<String>) {
        self.send_package(Reject {
            message: message.into(),
        })
        .await?;

        task::sleep(Duration::from_secs(2)).await;

        self.terminate().await?;
    }

    #[throws(anyhow::Error)]
    pub async fn end(&mut self) {
        self.send_package(End {}).await?;

        task::sleep(Duration::from_secs(2)).await;

        self.terminate().await?;
    }

    #[throws(anyhow::Error)]
    pub async fn call_incoming(&mut self, caller: SocketAddr) {
        let (remote_ip_v4, remote_ip_v6) = match caller.ip() {
            IpAddr::V4(addr) => (addr, Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0)),
            IpAddr::V6(addr) => (Ipv4Addr::new(0, 0, 0, 0), addr),
        };

        self.send_package(RemCall {
            remote_ip_v4,
            remote_ip_v6,
        })
        .await?;
    }
}
