use super::*;

use async_std::sync::Mutex;
use std::collections::HashMap;

lazy_static::lazy_static! {
    pub static ref AVAILABLE_PORTS: Mutex<Vec<Port>> = Mutex::new(Vec::new());
    pub static ref RESERVED_PORTS: Mutex<HashMap<ClientNumber, CancelableTask<anyhow::Result<Port>>>> = Mutex::new(HashMap::new());
    pub static ref UNAVAILABLE_PORTS: Mutex<Vec<UnavailablePort>> = Mutex::new(Vec::new());
}

/// Represents unique access to a Port.
pub struct Port {
    pub port: u16,
    pub listener: Option<TcpListener>,
}

impl std::fmt::Debug for Port {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Port ({})", self.port)
    }
}

impl Port {
    pub fn new(port: u16) -> Self {
        Self { port, listener: None }
    }
    pub fn unwrap(&self) -> u16 {
        self.port
    }

    pub fn close(&mut self) {
        // Replaces listener with None and drops the old listener
        let _listener = self.listener.take();
    }

    #[throws(Option<std::io::Error>)]
    pub async fn open_if_available(mut self) -> Port {
        debug!("checking if {:?} is available", self);
        self.close();
        let res = self.open().await;
        match res {
            Ok(()) => {
                debug!("{:?} is available", self);
                self
            }
            Err(err) if err.kind() == io::ErrorKind::AddrInUse => {
                debug!("{:?} is unavailable", self);
                self.unavailable().await;
                throw!(None);
            }
            Err(err) => {
                warn!("{:?} is temporarily unavailable. Io Error: {:?}", self, err);
                self.available().await;
                throw!(Some(err));
            }
        }
    }

    #[throws(io::Error)]
    pub async fn open(&mut self) {
        if self.listener.is_none() {
            let listener = TcpListener::bind(("0.0.0.0", self.port)).await?;
            self.listener = Some(listener);
        }
    }

    pub async fn is_open(&self) -> bool {
        self.listener.is_some()
    }

    pub async fn unavailable(mut self) {
        self.close();
        trace!("locking `UNAVAILABLE_PORTS`");
        UNAVAILABLE_PORTS.lock().await.push(UnavailablePort::new(self));
        trace!("unlocked `UNAVAILABLE_PORTS`");
        print_port_stats().await;
    }

    pub async fn available(mut self) {
        self.close();
        trace!("locking `AVAILABLE_PORTS`");
        AVAILABLE_PORTS.lock().await.push(self);
        trace!("unlocked `AVAILABLE_PORTS`");
        print_port_stats().await;
    }

    pub fn listener(&mut self) -> Option<&mut TcpListener> {
        self.listener.as_mut()
    }
}

impl Drop for Port {
    fn drop(&mut self) {
        warn!("dropped a port: {:?}", self);
        self.close();
        task::block_on(Port::new(self.port).available());
    }
}

pub async fn print_port_stats() {
    trace!("locking all port maps!");

    trace!("locking `AVAILABLE_PORTS`");
    let avail = AVAILABLE_PORTS.lock().await.len();
    trace!("unlocked `AVAILABLE_PORTS`");

    trace!("locking `UNAVAILABLE_PORTS`");
    let unavail = UNAVAILABLE_PORTS.lock().await.len();
    trace!("unlocked `UNAVAILABLE_PORTS`");

    trace!("locking `RESERVED_PORTS`");
    let reserved = RESERVED_PORTS.lock().await.len();
    trace!("unlocked `RESERVED_PORTS`");

    trace!("finished locking all port maps!");

    let total = PORT_RANGE.into_iter().count();
    let used = total - avail - unavail - reserved;
    info!(
        "[port stats]: available={avail} used={used} reserved={reserved} unavailable={unavail}",
        avail = avail,
        unavail = unavail,
        reserved = reserved,
        used = used
    );
}

pub fn spawn_unavailable_port_watchdog() {
    task::spawn(async {
        loop {
            task::sleep(Duration::from_secs(60)).await;

            let ports: Vec<UnavailablePort> = std::mem::replace(&mut *UNAVAILABLE_PORTS.lock().await, Vec::new());

            let mut unavailable_ports = Vec::new();
            let mut available_ports = Vec::new();

            for mut unavailable_port in ports {
                if unavailable_port.timed_out() {
                    debug!("checking if {:?} is now available", unavailable_port.port);
                    if unavailable_port.port.open().await.is_ok() {
                        debug!("{:?} is available", unavailable_port.port);

                        let mut port = unavailable_port.port;
                        port.close();
                        available_ports.push(port);
                    } else {
                        debug!(
                            "{:?} is unavailable trying again in {:?}",
                            unavailable_port.port, unavailable_port.timeout_duration
                        );

                        unavailable_ports.push(unavailable_port);
                    }
                }
            }

            let mut made_changes = false;
            if !available_ports.is_empty() {
                AVAILABLE_PORTS.lock().await.extend(available_ports);
                made_changes = true;
            }
            if !unavailable_ports.is_empty() {
                UNAVAILABLE_PORTS.lock().await.extend(unavailable_ports);
                made_changes = true;
            }

            if made_changes {
                print_port_stats().await;
            }
        }
    });
}

async fn get_available_port() -> Option<Port> {
    debug!("retrieving an available port from `AVAILABLE_PORTS`");

    trace!("locking `AVAILABLE_PORTS`");
    let port = AVAILABLE_PORTS.lock().await.pop();
    trace!("unlocked `AVAILABLE_PORTS`");

    print_port_stats().await;
    port
}

pub async fn get_port_for_number(number: ClientNumber) -> Option<Port> {
    debug!("getting a port for {:?}", number);

    trace!("locking reserved_ports");
    if let Some(port) = RESERVED_PORTS.lock().await.remove(&number) {
        trace!("unlocked reserved_ports");

        debug!("found a reserved port for {:?}", number);
        print_port_stats().await;
        return port.terminate().await.ok();
    }

    debug!("did not find a reserved port for {:?}.", number);

    get_available_port().await
}

pub async fn reserve_port(number: ClientNumber, mut port: Port, reject_message: impl Into<String>) {
    let reject_message = reject_message.into();

    info!(
        "reserving {:?} for {:?} with reject message: {:?}",
        port, number, reject_message
    );

    let (cancel, receiver) = channel(1);
    let handle = task::spawn(async move {
        trace!("starting port reservation for {:?}", port);

        port.open().await?;
        let listener = port.listener.as_ref().unwrap();

        trace!("got listener for {:?}", port);

        loop {
            trace!("top of reservation loop {:?}", port);
            futures::select! {
                _ = receiver.recv().fuse() => {
                    debug!("reservation for {:?} was cancelled", port);
                    break;
                },

                _ = task::sleep(PORT_RESERVATION_TIME).fuse() => {
                    if let Some(mut old) = RESERVED_PORTS.lock().await.remove(&number) {
                        old.cancel().await;
                    }
                    throw!(CentralexError::Timeout);
                },

                res = listener.accept().fuse() => {
                    info!("peer connected to reserved port");
                    let (peer, _) = res?;

                    let reject_message = reject_message.clone();

                    task::spawn(async move {
                        let mut client = Client::spawn(peer);
                        let _ = client.reject(reject_message).await;
                        let _ = client.finish().await;
                        // TODO: we don't really care about these Results,
                        // TODO: figure out if we should handle it anyways
                    });
                }
            }
        }

        Ok(port)
    });

    if let Some(old) = RESERVED_PORTS
        .lock()
        .await
        .insert(number, CancelableTask { handle, cancel })
    {
        if let Err(err) = old.terminate().await {
            error!("failed to terminate old port reservation: {:?}", err);
        }
    }

    print_port_stats().await;
}

pub struct UnavailablePort {
    port: Port,
    timeout: std::time::Instant,
    timeout_duration: std::time::Duration,
}

impl UnavailablePort {
    pub fn new(port: Port) -> Self {
        const INITIAL_TIMEOUT: Duration = Duration::from_secs(60);
        Self {
            port,
            timeout: Instant::now() + INITIAL_TIMEOUT,
            timeout_duration: INITIAL_TIMEOUT,
        }
    }

    pub fn bump_timeout(&mut self) {
        self.timeout_duration *= 2;
        self.timeout = Instant::now() + self.timeout_duration;
    }

    pub fn timed_out(&mut self) -> bool {
        if self.timeout >= Instant::now() {
            self.bump_timeout();
            true
        } else {
            false
        }
    }
}
