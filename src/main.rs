#![recursion_limit = "512"]

pub use anyhow::anyhow;
pub use async_std::{
    io, net,
    net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, TcpListener, TcpStream},
    prelude::*,
    sync::*,
    task::{self, JoinHandle},
};
pub use fehler::{throw, throws};
pub use futures::FutureExt;
pub use itelex::{centralex::*, Deserialize, Serialize};
pub use log::*;
pub use std::{
    collections::HashMap,
    time::{Duration, Instant},
};

pub mod cancelable_task;
pub mod client;
pub mod newtypes;
pub mod port_management;

pub use cancelable_task::CancelableTask;
pub use client::*;
pub use newtypes::*;
pub use port_management::*;

const PORT_RESERVATION_TIME: Duration = Duration::from_secs(60 * 60); // 1h
const CONNECTION_TIMEOUT: Duration = Duration::from_secs(15 * 60); // 15min
const HANDSHAKE_TIMEOUT: Duration = Duration::from_secs(30); // 30sec
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(15); // 15sec

#[derive(Debug, PartialEq, Eq)]
pub enum CentralexError {
    Disconnected,
    InvalidCredentials,
    Timeout,
    Rejected(String),
}

impl std::fmt::Display for CentralexError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CentralexError::Timeout => write!(f, "timed out"),
            CentralexError::Disconnected => write!(f, "disconnected"),
            CentralexError::InvalidCredentials => write!(f, "invalid credentials"),
            CentralexError::Rejected(reason) => write!(f, "client rejected the call with reason: {:?}", reason),
        }
    }
}
impl std::error::Error for CentralexError {}

const PORT_RANGE: std::ops::Range<u16> = 11900..12000;
const AUTH_SERVER: &'static str = "telexgateway.de:11811";

#[throws(std::io::Error)]
#[async_std::main]
async fn main() {
    simple_logger::init().unwrap();

    AVAILABLE_PORTS
        .lock()
        .await
        .extend(PORT_RANGE.into_iter().rev().map(|x| Port::new(x)));

    print_port_stats().await;

    spawn_unavailable_port_watchdog();

    let listener = TcpListener::bind(("0.0.0.0", 11818)).await?;
    while let Ok((socket, socket_addr)) = listener.accept().await {
        info!("got connection from {:?}", socket_addr);
        let _ = task::spawn(async move {
            match handle_client(socket).await {
                Err(err) => match err.downcast_ref::<CentralexError>() {
                    Some(reason @ CentralexError::Disconnected) | Some(reason @ CentralexError::Rejected(_)) => {
                        info!("client finished: {:?}", reason)
                    }
                    Some(err) => error!("client had an error: {:?}", err),
                    None => error!("client had an error: {:?}", err),
                },
                _ => debug!("client finished successfully"),
            }

            info!("connection closed");
        });
    }
}

#[throws(anyhow::Error)]
async fn handle_client(socket: TcpStream) {
    let mut client = Client::spawn(socket);

    // wait for connect package and return credentials
    let credentials = match client.receive_package().await {
        Some(Package::RemConnect(credentials)) => credentials,
        pkg @ _ => {
            let message = if let Some(pkg) = pkg {
                format!("unexpected package: {:?}", pkg)
            } else {
                "expected a package".into()
            };
            client.reject(&message).await?;
            throw!(anyhow!(message));
        }
    };

    let pin = Pin(credentials.pin);
    let number = ClientNumber(credentials.number);
    let mut port = 'port: loop {
        let port = match get_port_for_number(number).await {
            Some(port) => port,
            None => throw!(anyhow!("out of ports")),
        };
        debug!("got {:?} for {:?}", port, number);

        if let Err(err) = authenticate_client(number, &port, pin).await {
            match err.downcast_ref::<CentralexError>() {
                Some(err) if *err == CentralexError::InvalidCredentials => {
                    warn!("{:?} tried to authenticate with invalid credentials", number);
                    client.reject("na").await?;
                }
                _ => {}
            }
            throw!(err);
        }

        // NOTE: we do this after authenticating, so that an unauthenticated client can not
        // use up server resources.
        debug!("checking if {:?} for {:?} is actually available", port, number);
        match port.open_if_available().await {
            Ok(port) => break 'port port, // return the port if it was available
            Err(err) => match err {
                None => continue 'port,   // try again if the port was unavailable
                Some(err) => throw!(err), // return with an io error if there was one
            },
        }
    };

    client
        .send_package(RemConfirm {})
        .await
        .map_err(|_| anyhow!("failed to send package"))?;

    let listener = port.listener().expect("port must be open at this point");

    info!("waiting for peer");

    let (peer, peer_addr) = futures::select! {
        package = client.receive_package().fuse() => {
            match package {
                Some(Package::End(_)) => {
                    reserve_port(number, port, "na").await;
                },
                pkg @ _ => {
                    let message = if let Some(pkg) = pkg {
                        format!("unexpected package: {:?}", pkg)
                    } else {
                        "expected a package".into()
                    };
                    client.reject(&message).await?;
                    throw!(anyhow!(message));
                }
            }

            client.finish().await?;
            return Ok(());
        },
        peer = listener.accept().fuse() => {
            peer?
        }
    };

    debug!("peer has connected; signaling client");

    client.call_incoming(peer_addr).await?;

    match client.receive_package().await {
        Some(Package::RemAck(_)) => {
            debug!("the client accepted the call");
        }
        Some(Package::End(_)) => {
            let res: anyhow::Result<()> = futures::join! {
                async {
                    let mut peer = Client::spawn(peer);
                    peer.end().await?;

                    Ok(())
                },
                reserve_port(number, port, "na"),
                client.finish(),
            }
            .0;
            return res?;
        }
        Some(Package::Reject(Reject { message })) => {
            debug!("the client rejected the call with message: {:?}", message);

            let res: anyhow::Result<()> = futures::join! {
                async {
                    let mut peer = Client::spawn(peer);
                    peer.reject(&message).await?;

                    throw!(CentralexError::Rejected(message.clone()));
                },
                reserve_port(number, port, &message),
                client.finish(),
            }
            .0;

            if let Err(err) = res {
                throw!(err);
            } else {
                unreachable!("the above always fails");
            }
        }
        pkg @ _ => {
            let message = if let Some(pkg) = pkg {
                format!("unexpected package: {:?}", pkg)
            } else {
                "expected a package".into()
            };
            client.reject(&message).await?;
            throw!(anyhow!(message));
        }
    }

    info!("connecting client with peer");

    let client = client.finish().await?;

    let mut peer_writer = &peer;
    let mut peer_reader = io::BufReader::new(&peer);

    let mut client_writer = &client;
    let mut client_reader = io::BufReader::new(&client);

    loop {
        futures::select! {
            _ = task::sleep(CONNECTION_TIMEOUT).fuse() => {
                info!("connection timed out");
                throw!(CentralexError::Timeout);
            },

            n = io::copy(&mut client_reader, &mut peer_writer).fuse() => {
                match n {
                    Ok(n) if n!=0 => {
                        debug!("read {} bytes from client", n);
                    },
                    _ => break
                }
            },

            n = io::copy(&mut peer_reader, &mut client_writer).fuse() => {
                match n {
                    Ok(n) if n!=0 => {
                        debug!("read {} bytes from peer", n);
                    },
                    _ => break
                }
            },
        }
    }
}

#[throws(anyhow::Error)]
async fn authenticate_client(number: ClientNumber, port: &Port, pin: Pin) {
    info!("authenticating {:?} for {:?}", number, port);

    let mut socket = TcpStream::connect(AUTH_SERVER).await?;

    let package: itelex::server::Package = itelex::server::ClientUpdate {
        number: number.unwrap(),
        pin: pin.unwrap(),
        port: port.unwrap(),
    }
    .into();

    let mut buffer: Vec<u8> = Vec::with_capacity(2);
    let mut cursor = std::io::Cursor::new(&mut buffer);

    package.serialize_le(&mut cursor)?;
    socket.write_all(&buffer).await?;

    let mut header = [0_u8; 2];

    // TODO: don't require the server to send both header bytes in one package
    if socket.peek(&mut header).await? < 2 {
        warn!("server send too little header data");
    }

    // debug!("header: {:?}", header);
    let [_package_type, package_length] = header;

    let mut buffer = vec![0_u8; package_length as usize + 2];

    socket.read_exact(&mut buffer).await?;

    let package = itelex::server::Package::deserialize_le(&mut std::io::Cursor::new(buffer))?;

    if let itelex::server::Package::AddressConfirm(_) = package {
        info!("auth was valid");
    } else {
        warn!("auth was invalid");
        throw!(CentralexError::InvalidCredentials)
    }
}
