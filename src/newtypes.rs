#[derive(Hash, Eq, Copy, Clone, Debug)]
pub struct ClientNumber(pub u32);
impl PartialEq for ClientNumber {
    fn eq(&self, other: &ClientNumber) -> bool {
        self.0 == other.0
    }
}
impl ClientNumber {
    pub fn unwrap(&self) -> u32 {
        self.0
    }
}

#[derive(Hash, Eq, Copy, Clone, Debug)]
pub struct Pin(pub u16);
impl PartialEq for Pin {
    fn eq(&self, other: &Pin) -> bool {
        self.0 == other.0
    }
}
impl Pin {
    pub fn unwrap(&self) -> u16 {
        self.0
    }
}
